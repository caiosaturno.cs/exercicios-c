#include <stdio.h>
#include <stdlib.h>
// Caio Cesar Loreno Saturno - Questao 1
int main(){
    int escolha = 0;
    while (escolha >= 0){
        printf("\nMenu:\n1 - Opcao 1\n2 - Opcao 2\n3 - Opcao 3\n4 - Opcao 4\n5 - Opcao 5\n6 - Opcao 6\n7 - Opcao 7\n8 - Opcao 8\n9 - Opcao 9\n10 - Sair\n\nOpcao: ");
        scanf("%d", &escolha);
        switch (escolha){
            case 0:
                printf("Fim do programa, Obrigado por utilizar o programa!\n");
                break;
            case 1:
                printf("Voce selecionou a opcao 1!\n");
                escolha = 0;
                break;    
            case 2:
                printf("Voce selecionou a opcao 2!\n");
                escolha = 0;
                break;
            case 3:
                printf("Voce selecionou a opcao 3!\n");
                escolha = 0;
                break;
            case 4:
                printf("Voce selecionou a opcao 4!\n");
                escolha = 0;
                break;
            case 5:
                printf("Voce selecionou a opcao 5!\n");
                escolha = 0;
                break;
            case 6:
                printf("Voce selecionou a opcao 6!\n");
                escolha = 0;
                break;
            case 7:
                printf("Voce selecionou a opcao 7!\n");
                escolha = 0;
                break;
            case 8:
                printf("Voce selecionou a opcao 8!\n");
                escolha = 0;
                break;
            case 9:
                printf("Voce selecionou a opcao 9!\n");
                escolha = 0;
                break;
            default:
                printf("Opcao selecionada incorreta!\n\n");
                escolha = 0;
                break;
        }
        
    }    
    return 0;
}
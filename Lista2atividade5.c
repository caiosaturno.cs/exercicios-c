#include <stdlib.h>
#include <stdio.h>
/* lista atividade 2 - Questao 5
Caio Cesar, ADS - noite*/
int main(){
    int a0, razao, n;
    int cont;
    int termo;
    printf("informe dados(inteiros) para: razao da PA e o numero de termos: \n");
    scanf("%d %d", &razao, &n);
    cont = 0;
    a0 = 0;
    while (cont < n){
       termo = a0 + cont * razao;
       printf("%d ", termo);
       cont++;
    }
    printf("\n");
return 0;
}
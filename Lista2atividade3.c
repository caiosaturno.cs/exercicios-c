#include <stdio.h>
#include <stdlib.h>
// Caio Cesar Loreno Saturno - Questao 3
int main(){
    int fatorial;
    int num;
    printf("Digite um numero para saber seu fatorial: ");
    scanf("%d", &num);
    for (fatorial = 1; num > 1; num = num - 1){
        fatorial = fatorial*num;
    }
    printf("O numero fatorial e: %d", fatorial);
    return 0;
}
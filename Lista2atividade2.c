#include <stdio.h>
#include <stdlib.h>
// Caio Cesar Loreno Saturno - Questao 2
int main(){
    int num;
    printf("Digite um numero (inteiro) desejado: ");
    scanf("%d", &num);
    if(num%2==0){
        printf("Numero escolhido e par!\n");
    }else{
        printf("Numero escolhido e impar!\n");
    } 
    printf("Fim do programa!");
    return 0;
}